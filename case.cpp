#include <iostream>
#include "case.h"

using namespace std;

//Constructeurs
Case::Case() : numero(-1), color(GRISE){
	groupement = Coord();}

Case::Case(const int num,int x,int y) : numero(num){
	if(numero > 0){
		color = BLANCHE;
		groupement = Coord(x,y);
	}
	else
		color = GRISE;
}

//Setters and getters
const int Case::getNumero() const{
	return numero;
}

Case::Couleur Case::getCouleur() const{
	return color;
}



void Case::setCoordCase(int x,int y){
	coordCase.setX(x);
	coordCase.setY(y);
}

Coord Case::getCoordCase() {return coordCase;}


void Case::changeCouleur(Case::Couleur c){
	color = c;
}

void Case::changeNumero(int newNumero){
	numero = newNumero;
	color = BLANCHE;
}

int Case::getCoordX(){
	return getCoordCase().getX();
}

int Case::getCoordY(){
	return getCoordCase().getY();
}

Coord Case::getCoordCaseGroupement(){
	return groupement;
}
/*
void Case::setCoordCaseGroupement(Coord c){
	Coord foo=c.getCoordCaseGroupement();
	groupement.setX(c.getX());
	groupement.setY(c.getY());
}*/