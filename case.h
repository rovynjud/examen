#ifndef CASE_H
#define CASE_H
#include "Coord.h"

class Case{

public:
	enum Couleur{
		NOIRE,
		BLANCHE,
		GRISE			//Pour les cases indéterminées
	};

private:
	int numero;
	Couleur color;
	Coord groupement; //Coord de la case numérotée
	Coord coordCase;

public:

	//Constructeurs
	Case();							//Constructeur par défault, numéro initialisé à -1 (ce n'est donc pas une cas à numéro)
	Case(const int num, int x, int y);			//Constructeur paramétré. Si numero>0 alors color = BLANCHE, sinon color = GRISE

	//Setters and getters
	const int getNumero() const;
	Couleur getCouleur() const;

	void setCoordCase(int x,int y);
	Coord getCoordCase();
	int getCoordX();
	int getCoordY();
	Coord getCoordCaseGroupement();
	void setCoordCaseGroupement(Coord c);


	void changeCouleur(Couleur c);	//Méthode pour changer la couleur de la case
	void changeNumero(int newNumero);

};

#endif
