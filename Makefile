COMPIL = g++ -Wall -c -std=c++14
COMPILEXEC = g++ -Wall -std=c++14
 
all: 
	$(COMPIL) Grille.cpp case.cpp outils-grille.cpp Coord.cpp
	$(COMPILEXEC) test-main.cpp outils-grille.o case.o Grille.o Coord.o -o test

outils-grille.o : outils-grille.cpp
	$(COMPIL) outils-grille.cpp
	
Grille.o : Grille.cpp
	$(COMPIL) Grille.cpp case.cpp Coord.cpp

case.o : case.cpp 
	$(COMPIL) Coord.cpp case.cpp 

Coord.o : Coord.cpp
	$(COMPIL) Coord.cpp




	
