#include "Grille.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

// Constructeurs
	Grille::Grille():ligne(5),colonne(5){

	grille = new Case*[colonne];
	for (int x = 0; x< colonne; x++){
		grille[x] = new Case[ligne];}

		for (int x = 0; x< ligne; x++ ){
			for (int y =0;y < colonne; y++){

				grille[x][y].setCoordCase(x,y);
			}
		}

		grille[0][2].changeNumero(2);
		grille[2][2].changeNumero(2);
		grille[1][0].changeNumero(1);
		grille[3][0].changeNumero(2);
		grille[2][4].changeNumero(1);
		grille[4][3].changeNumero(2);



	}

// Accesseurs
	const int Grille::getLigne(){return ligne; }
	const int Grille::getColonne() {return colonne; }

int Grille::nbrCaseProchesGrises_V1(int x, int y){
	int compteur = 0;
	if (estDansGrille(x,y-1)){ if (estGrise(x, y-1)) compteur++;}
	if (estDansGrille(x,y+1)){ if (estGrise(x, y+1)) compteur++;}
	if (estDansGrille(x-1,y)){ if (estGrise(x-1, y)) compteur++;}
	if (estDansGrille(x+1,y)){ if (estGrise(x+1,y)) compteur++;}
	return compteur;
}
//// Test Bool
bool Grille::estBlanche(int x, int y) {
	return grille[x][y].getCouleur() == Case::Couleur::BLANCHE;
}

bool Grille::estNoire(int x, int y) {
	return grille[x][y].getCouleur() == Case::Couleur::NOIRE;
}

bool Grille::estGrise(int x, int y) {
	return grille[x][y].getCouleur() == Case::Couleur::GRISE;
}

bool Grille::estDansGrille(int x, int y){
	return (x>=0 && x<ligne && y>=0 && y<ligne);
}

void Grille::quelCouleurTerminal(int x, int y){
	cout << "La case [" << x << "] [" << y << "] est ";
	if (grille[x][y].getCouleur() == Case::Couleur::BLANCHE) cout << " Blanche " << endl ;
	if (grille[x][y].getCouleur() == Case::Couleur::NOIRE) cout << " Noire " << endl ;
	if (grille[x][y].getCouleur() == Case::Couleur::GRISE) cout << " Grise " << endl ;
}

void Grille::casUn(int x, int y){
	if(grille[x][y].getNumero() == 1){
		if(estDansGrille(x+1,y)){
			grille[x+1][y].changeCouleur(Case::Couleur::NOIRE);
		}

		if(estDansGrille(x-1,y)){
			grille[x-1][y].changeCouleur(Case::Couleur::NOIRE);
		}

		if(estDansGrille(x,y+1)){
			grille[x][y+1].changeCouleur(Case::Couleur::NOIRE);
		}

		if(estDansGrille(x,y-1)){
			grille[x][y-1].changeCouleur(Case::Couleur::NOIRE);
		}
	}
}

void Grille::rechercheCaseNonAtteignable(int x, int y, int d, bool** grilleBool){
	if(d != 0){
		grilleBool[x][y] = true;

		if(estDansGrille(x+1,y) && grilleBool[x+1][y] != true){
			rechercheCaseNonAtteignable(x+1,y,d-1,grilleBool);
		}

		if(estDansGrille(x-1,y) && grilleBool[x-1][y] != true){
			rechercheCaseNonAtteignable(x-1,y,d-1,grilleBool);
		}

		if(estDansGrille(x,y+1) && grilleBool[x][y+1] != true){
			rechercheCaseNonAtteignable(x,y+1,d-1,grilleBool);
		}

		if(estDansGrille(x,y-1) && grilleBool[x][y-1] != true){
			rechercheCaseNonAtteignable(x,y-1,d-1,grilleBool);
		}
	}
}

void Grille::premierParcours(){
	//Création de la grille de booléens
	bool** grilleBool = new bool*[colonne];
	for(int i = 0; i < colonne; i++){
		grilleBool[i] = new bool[ligne];
	}

	//Initialisations de la grille de booléens à false
	for(int i = 0; i < colonne; i++){
		for(int j = 0; j < ligne; j++){
			grilleBool[i][j] = false; // pour debug autre chose que rechercheCaseNonAtteignable, passer à True, remettre à false après
		}
	}

	for(int i = 0; i < colonne; i++){
		for(int j = 0; j < ligne; j++){
			if(grille[i][j].getNumero() > 0){
				casUn(i,j);
				rechercheCaseNonAtteignable(i,j,grille[i][j].getNumero(),grilleBool);
				rechercheEntreDeux(i,j);
			}
		}
	}

	for(int i = 0; i < colonne; i++){
		for(int j = 0; j < ligne; j++){
			if(grilleBool[i][j] == false){
				grille[i][j].changeCouleur(Case::Couleur::NOIRE);
			}
		}
	}
	for(int i=0;i<colonne;i++){
		delete[] grilleBool[i];
	}
	delete[] grilleBool;
}

void Grille::rechercheEntreDeux(int x, int y) {
	// Cas dans l'axe
	if (estDansGrille(x+2,y) && grille[x+2][y].getNumero()>0){
		grille[x+1][y].changeCouleur(Case::Couleur::NOIRE);
	}

	if (estDansGrille(x-2,y) && grille[x-2][y].getNumero()>0){
		grille[x-1][y].changeCouleur(Case::Couleur::NOIRE);
	}

	if (estDansGrille(x,y+2) && grille[x][y+2].getNumero()>0){
		grille[x][y+1].changeCouleur(Case::Couleur::NOIRE);
	}

	if (estDansGrille(x,y-2) && grille[x][y-2].getNumero()>0){
		grille[x][y-1].changeCouleur(Case::Couleur::NOIRE);
	}

	// Cas en diagonale
	if (estDansGrille(x-1,y-1) && grille[x-1][y-1].getNumero()>0){
		grille[x-1][y].changeCouleur(Case::Couleur::NOIRE);
		grille[x][y-1].changeCouleur(Case::Couleur::NOIRE);
	}
	if (estDansGrille(x-1,y+1) && grille[x-1][y+1].getNumero()>0){
		grille[x-1][y].changeCouleur(Case::Couleur::NOIRE);
		grille[x][y+1].changeCouleur(Case::Couleur::NOIRE);
	}
	if (estDansGrille(x+1,y-1) && grille[x+1][y-1].getNumero()>0){
		grille[x+1][y].changeCouleur(Case::Couleur::NOIRE);
		grille[x][y-1].changeCouleur(Case::Couleur::NOIRE);
	}
	if (estDansGrille(x+1,y+1) && grille[x+1][y+1].getNumero()>0){
		grille[x+1][y].changeCouleur(Case::Couleur::NOIRE);
		grille[x][y+1].changeCouleur(Case::Couleur::NOIRE);
	}
}


bool Grille::entoureParBlancOuVide(int x , int y){// renvoie true si la case est entourés de blancs ou de vide, sauf une case 


	int cmpt=0;
	if(!estDansGrille(x+1,y) || estBlanche(x+1,y)){
		cmpt++;
	}
	if(!estDansGrille(x-1,y) || estBlanche(x-1,y)){
		cmpt++;
	}
	if(!estDansGrille(x,y+1) || estBlanche(x,y+1)){
		cmpt++;
	}
	if(!estDansGrille(x,y-1) || estBlanche(x,y-1)){
		cmpt++;
	}

	return cmpt==3;
}

void Grille::isolementCaseNoire(int x, int y){
	if(entoureParBlancOuVide(x,y)){
		if(estDansGrille(x+1,y) && estGrise(x+1,y))
		grille[x+1][y].changeCouleur(Case::Couleur::NOIRE);

		if(estDansGrille(x,y+1) && estGrise(x,y+1))
		grille[x][y+1].changeCouleur(Case::Couleur::NOIRE);

		if(estDansGrille(x-1,y) && estGrise(x-1,y))
		grille[x-1][y].changeCouleur(Case::Couleur::NOIRE);

		if(estDansGrille(x,y-1) && estGrise(x,y-1))
		grille[x][y-1].changeCouleur(Case::Couleur::NOIRE);
	}
}


bool Grille::entoureParNoirOuVide(int x , int y){// renvoie true si la case est entourés de blancs ou de vide, sauf une case 

	int cmpt=0;
	if(!estDansGrille(x+1,y) || estNoire(x+1,y)){
		cmpt++;
	}
	//else if (estDansGrille(x+1,y) && estGrise(x+1,y) && blancOuVide(x+1,y)) cmpt++;

	if(!estDansGrille(x-1,y) || estNoire(x-1,y)){
		cmpt++;
	}
	//else if (estDansGrille(x-1,y) && estGrise(x-1,y) && blancOuVide(x-1,y)) cmpt++;

	if(!estDansGrille(x,y+1) || estNoire(x,y+1)){
		cmpt++;
	}
	//else if (estDansGrille(x,y+1) && estGrise(x,y+1) && blancOuVide(x,y+1)) cmpt++;

	if(!estDansGrille(x,y-1) || estNoire(x,y-1)){
		cmpt++;
	}
	//else if (estDansGrille(x,y-1) && estGrise(x,y-1) && blancOuVide(x,y-1)) cmpt++;

	return cmpt==3;
}

void Grille::isolementCaseBlanche(int x, int y){
	if(entoureParNoirOuVide(x,y)){
		if(estDansGrille(x+1,y) && estGrise(x+1,y))
		grille[x+1][y].changeCouleur(Case::Couleur::BLANCHE);

		if(estDansGrille(x,y+1) && estGrise(x,y+1))
		grille[x][y+1].changeCouleur(Case::Couleur::BLANCHE);

		if(estDansGrille(x-1,y) && estGrise(x-1,y))
		grille[x-1][y].changeCouleur(Case::Couleur::BLANCHE);

		if(estDansGrille(x,y-1) && estGrise(x,y-1))
		grille[x][y-1].changeCouleur(Case::Couleur::BLANCHE);
	}

}


//prend les coord d'une case grise, le test a été fait
void Grille::carreNoir(int x,int y){
	//zone 1
	if(estDansGrille(x-1,y) && estNoire(x-1,y)){
		if(estDansGrille(x-1,y-1) && estNoire(x-1,y-1)){
			if(estDansGrille(x,y-1) && estNoire(x,y-1)){
				grille[x][y].changeCouleur(Case::Couleur::BLANCHE);
			}
		}
	}
	//zone 2
	if(estDansGrille(x+1,y) && estNoire(x+1,y)){
		if(estDansGrille(x,y-1) && estNoire(x,y-1)){
			if(estDansGrille(x+1,y-1) && estNoire(x+1,y-1)){
				grille[x][y].changeCouleur(Case::Couleur::BLANCHE);
			}
		}
	}
	// 3
	if(estDansGrille(x,y+1) && estNoire(x,y+1)){
		if(estDansGrille(x+1,y) && estNoire(x+1,y)){
			if(estDansGrille(x+1,y+1) && estNoire(x+1,y+1)){
				grille[x][y].changeCouleur(Case::Couleur::BLANCHE);
			}
		}
	}
	// quatre
	if(estDansGrille(x,y+1) && estNoire(x,y+1)){
		if(estDansGrille(x-1,y) && estNoire(x-1,y)){
			if(estDansGrille(x-1,y+1) && estNoire(x-1,y+1)){
				grille[x][y].changeCouleur(Case::Couleur::BLANCHE);
			}
		}
	}
}



void Grille::afficheDEBUG(){
	for (int x = 0; x< ligne; x++ ){
		for (int y =0;y < colonne; y++){
			if (grille[x][y].getNumero() > 0) cout << " " << grille[x][y].getNumero() << " ";
			else if (estBlanche(x,y)) cout << " B ";
			else if (estNoire(x,y)) cout << " N ";
			else if (estGrise(x,y)) cout << " G ";
		}
		cout << endl;
	}
}

void Grille::imprimerGrille(char const * nomFichier){

	int nbrColonne=this->colonne;
	int nbrLigne=this->ligne;

	int tailleTab=nbrColonne*nbrLigne+nbrLigne;
	char * tableauEcriture=new char[tailleTab];
	int indiceEcriture=0;
	for(int i=0;i<nbrLigne;i++)
	{
		if(i!=0){
			tableauEcriture[indiceEcriture]=':';
			indiceEcriture++;
		}
		for(int j=0;j<nbrColonne;j++)
		{
			if(estBlanche(i,j))
			{
				if(grille[i][j].getNumero()==-1){
					tableauEcriture[indiceEcriture]='B';
				}
				else{
					tableauEcriture[indiceEcriture]=grille[i][j].getNumero();
				}
			}
			else if(estNoire(i,j)){
				tableauEcriture[indiceEcriture]='N';
			}
			else if(estGrise(i,j)){
				tableauEcriture[indiceEcriture]='?';
			}

			indiceEcriture++;
		}
	}
	tableauEcriture[indiceEcriture]='\n';

	ofstream fichier;
	fichier.open(nomFichier,ios_base::app);
	fichier.write(tableauEcriture,tailleTab);
	fichier.close();
}

void Grille::marquerCasesPotentielles(bool ** grilleBool,Case& c){
	int x=c.getCoordX();
	int y=c.getCoordY();
	grilleBool[x][y]=true;

	if(estDansGrille(x+1,y) && !grilleBool[x+1][y]){
		if(estNoire(x+1,y)){
			marquerCasesPotentielles(grilleBool, grille[x+1][y]);
		}
		else if(estGrise(x+1,y)){
			grilleBool[x+1][y]=true;
		}
	}

	if(estDansGrille(x-1,y) && !grilleBool[x-1][y]){
		if(estNoire(x-1,y)){
			marquerCasesPotentielles(grilleBool,grille[x-1][y]);
		}
		else if(estGrise(x-1,y)){
			grilleBool[x-1][y]=true;
		}
	}

	if(estDansGrille(x,y+1) && !grilleBool[x][y+1]){
		if(estNoire(x,y+1)){
			marquerCasesPotentielles(grilleBool,grille[x][y+1]);
		}
		else if(estGrise(x,y+1)){
			grilleBool[x][y+1]=true;
		}
	}

	if(estDansGrille(x,y-1) && !grilleBool[x][y-1]){
		if(estNoire(x,y-1)){
			marquerCasesPotentielles(grilleBool,grille[x][y-1]);
		}
		else if(estGrise(x,y-1)){
			grilleBool[x][y-1]=true;
		}
	}
}

void Grille::etendreLigneNoire(Case& c){
	bool ** grilleBool=new bool*[ligne];
	for(int i=0;i<ligne;i++){
		grilleBool[i]=new bool[colonne];
	}

	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			grilleBool[i][j]=false;
		}
	}

	marquerCasesPotentielles(grilleBool,c);

	int compteurCasePossibles=0;
	int coordCaseX=0;
	int coordCaseY=0;
	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			if(grilleBool[i][j] && estGrise(i,j)){
				compteurCasePossibles++;
				coordCaseX=i;
				coordCaseY=j;
			}
		}
	}
	if(compteurCasePossibles==1){
		devientNoire(coordCaseX,coordCaseY);
	}
}

void Grille::devientNoire(int x, int y){
	grille[x][y].changeCouleur(Case::Couleur::NOIRE);
}

void Grille::devientBlanche(int x, int y){
	grille[x][y].changeCouleur(Case::Couleur::BLANCHE);
}

void Grille::parcoursV1(){

	for (int x = 0; x< ligne; x++ ){
		for (int y =0;y < colonne; y++){
			if (estNoire(x,y)) etendreLigneNoire(grille[x][y]);
			else if (estGrise(x,y)) carreNoir(x,y);

			if (estGrise(x,y)) isolementCaseNoire(x,y);
			if (estBlanche(x,y)) isolementCaseBlanche(x,y);
		}
		
	}
}
/*
void chercheGroupement(int x, int y){
	int posX=x+1;
	int posY=y;

	if(grille[posX][posY].getCouleur==Case::Couleur::BLANCHE){
		grille[x][y].setCoordCaseGroupement()
	}
}
*/