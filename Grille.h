#ifndef GRILLE_H
#define GRILLE_H

#include "case.h"
#include <iostream>
#include <string>


class Grille {
private:
	Case** grille;  // ensemble de toutes les cases de la grille
	const int ligne;
	const int colonne;
	int nbrCasesBlanchesGrille;
	int tailleFinaleCaseNoire;


public:
//////// Constructeurs
    Grille();
	Grille(std::string adresse,int ligne,int colonne); // constructeur par envoie de grille, appel de chargementGrille() dans "outils-grille"


/////// Accesseurs Lecture et écriture
	const int getLigne();
	const int getColonne();

	//Case getCaseGrille (int x, int y); // renvoit la case coordonnées (x,y)


	int nbrCaseProchesGrises_V1(int x, int y);


///////// Méthodes de modifications de cases, utilisé pour faire appeller des cases alentours. -- A modifier -- 
    void devientNoire(int x, int y);
    void devientBlanche(int x, int y);
    	void chercheGroupement(int x, int y);


///////// Test booleen sur les cases 
	bool estBlanche(int x, int y);
	bool estNoire(int x, int y);
	bool estGrise(int x, int y);
	bool estDansGrille(int x, int y);


//////Fonctions  de Debug
	void quelCouleurTerminal(int x, int y); // renvoit les coordoonées suivi de la couleur de la case dans le terminal
	void afficheDEBUG();

////Méthodes de résolution
	////Parcours des cas de Départ 
		void premierParcours();  
			void casUn(int x, int y);
			void rechercheCaseNonAtteignable(int x, int y, int d, bool** grilleBool);
			void rechercheEntreDeux(int x, int y);

	/////Parcours généraux
		void parcoursV1();	
			//Sur Case Grise
				void isolementCaseNoire(int x, int y); 
					bool entoureParBlancOuVide(int x , int y);
				void carreNoir(int x, int y);
			//Sur Case Noire
				void etendreLigneNoire(Case& caseNoire);	
					void marquerCasesPotentielles(bool ** grille,Case& c);
			//Sur Case Blanche
				void isolementCaseBlanche(int x, int y); 
					bool entoureParNoirOuVide(int x , int y);


//Méthode liés à Qt
	void imprimerGrille(char const * nomFichier);



};

#endif
