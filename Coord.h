#ifndef COORD_H
#define COORD_H

class Coord{
 private:
   int x;
   int y;

 public:

  Coord();
  Coord(int,int);

  int getX();
  int getY();
  void setX(int);
  void setY(int); 

  bool estVoisin(int,int);
  
};

#endif
