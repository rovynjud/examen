CONFIG += c++14
*gcc*:QMAKE_CXXFLAGS += -std=c++11
QT += widgets

SOURCES += \
    main.cpp \
    fenetrenurikabe.cpp

HEADERS += \
    fenetrenurikabe.h

DISTFILES += \
    grille.txt
