#include "fenetrenurikabe.h"
#include <string>

FenetreNurikabe::FenetreNurikabe(int nbCaseParLigne) : QWidget()
{
    //on set les dimensions de la grande fenêtre en comptant un espace entre chaque QPButton
    //  (taille d'un button : 25x)
    setFixedSize(nbCaseParLigne*26,nbCaseParLigne*26);

    //Allocation dynamique de l'espace mémoire requis
    tableauCase=new QPushButton**[nbCaseParLigne];
    for(int i=0;i<nbCaseParLigne;i++)
    {
        tableauCase[i]=new QPushButton*[nbCaseParLigne];
        for(int j=0;j<nbCaseParLigne;j++)
        {
            tableauCase[i][j]=new QPushButton(this);
        }
    }

    //on donne à chaque QPButton les attributs que l'on souhaite.
    int coordX=0,coordY=0;
    int mosaique=0;
    for(int i=0;i<nbCaseParLigne;i++)
    {
        for(int j=0;j<nbCaseParLigne;j++)
        {
            //taille
            tableauCase[i][j]->setFixedSize(25,25);
            //couleur et bordure
            if(mosaique%2)
            {
            tableauCase[i][j]->setStyleSheet("background-color: black;"
                                          "border: 1px solid black;");
            }
            else
            {
                tableauCase[i][j]->setStyleSheet("background-color: white;"
                                              "border: 1px solid black;");
            }
            //on place le bouton sur la fenêtre
            tableauCase[i][j]->move(coordX,coordY);

            //coordX prend la valeur de la coord X du prochain bouton
            coordX+=26;
            tableauCase[i][j]->setText("");
            mosaique++;
        }
        coordX=0;
        coordY+=26; //on a fini une ligne, coordY doit donc s'incrémenter et coordX est reinit à 0
    }
}
FenetreNurikabe::FenetreNurikabe(int nbCaseParLigne,int nbCaseParColonne) : QWidget()
{
    //on set les dimensions de la grande fenêtre en comptant un espace entre chaque QPButton
    //  (taille d'un button : 25x)
    setFixedSize(nbCaseParLigne*26,nbCaseParColonne*26);

    //Allocation dynamique de l'espace mémoire requis
    tableauCase=new QPushButton**[nbCaseParColonne];
    for(int i=0;i<nbCaseParColonne;i++)
    {
        tableauCase[i]=new QPushButton*[nbCaseParLigne];
        for(int j=0;j<nbCaseParLigne;j++)
        {
            tableauCase[i][j]=new QPushButton(this);
        }
    }

    //on donne à chaque QPButton les attributs que l'on souhaite.
    int coordX=0,coordY=0;
    int mosaique=1;
    for(int i=0;i<nbCaseParColonne;i++)
    {
        for(int j=0;j<nbCaseParLigne;j++)
        {
            //taille
            tableauCase[i][j]->setFixedSize(25,25);
            //couleur et bordure
            if(mosaique%2)
            {
                tableauCase[i][j]->setStyleSheet("background-color: black;"
                                          "border: 1px solid black;");
            }
            else
            {
                tableauCase[i][j]->setStyleSheet("background-color: white;"
                                              "border: 1px solid black;");
            }
            //on place le bouton sur la fenêtre
            tableauCase[i][j]->move(coordX,coordY);

            //coordX prend la valeur de la coord X du prochain bouton
            coordX+=26;
            tableauCase[i][j]->setText("1");
            mosaique++;
        }
        coordX=0;
        coordY+=26; //on a fini une ligne, coordY doit donc s'incrémenter et coordX est reinit à 0
    }
}


FenetreNurikabe::FenetreNurikabe(int nbCaseParLigne,int nbCaseParColonne,int ** grille) : QWidget()
{
    std::string contenuCase;
    QString caseToQS;
    //on set les dimensions de la grande fenêtre en comptant un espace entre chaque QPButton
    //  (taille d'un button : 25x)
    setFixedSize(nbCaseParLigne*26,nbCaseParColonne*26);

    //Allocation dynamique de l'espace mémoire requis
    tableauCase=new QPushButton**[nbCaseParColonne];
    for(int i=0;i<nbCaseParColonne;i++)
    {
        tableauCase[i]=new QPushButton*[nbCaseParLigne];
        for(int j=0;j<nbCaseParLigne;j++)
        {
            tableauCase[i][j]=new QPushButton(this);
        }
    }
    /*
 * std::string contenuCase;
    contenuCase.push_back(lecteurCase);
    QString caseToQS=QString::fromStdString(s);
    */
    //on donne à chaque QPButton les attributs que l'on souhaite.
    int coordX=0,coordY=0;
    for(int i=0;i<nbCaseParColonne;i++)
    {
        for(int j=0;j<nbCaseParLigne;j++)
        {
            //taille
            tableauCase[i][j]->setFixedSize(25,25);
            //couleur et bordure
            if(grille[i][j] == -1)
            {
                tableauCase[i][j]->setStyleSheet("background-color: black;"
                                                 "border: 1px solid black;");
            }
            else
            {
                if(grille[i][j] != 0 && grille[i][j] != -2)
                {
                    contenuCase=std::to_string(grille[i][j]);
                    caseToQS=QString::fromStdString(contenuCase);
                    tableauCase[i][j]->setText(caseToQS);
                }
                if(grille[i][j] == -2){
                    tableauCase[i][j]->setText("?");
                    tableauCase[i][j]->setStyleSheet("color: white;"
                                                    "background-color: grey;"
                                                    "border: 1px solid black;");
                }
                else
                    tableauCase[i][j]->setStyleSheet("background-color: white;"
                                                    "border: 1px solid black;");
            }
            //on place le bouton sur la fenêtre
            tableauCase[i][j]->move(coordX,coordY);

            //coordX prend la valeur de la coord X du prochain bouton
            coordX+=26;
        }
        coordX=0;
        coordY+=26; //on a fini une ligne, coordY doit donc s'incrémenter et coordX est reinit à 0
    }
}
