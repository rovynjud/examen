#ifndef FENETRENURIKABE_H
#define FENETRENURIKABE_H
#include <QApplication>
#include <QWidget>
#include <QPushButton>


class FenetreNurikabe : public QWidget
{
    Q_OBJECT
private:
    QPushButton ***tableauCase;
public:
    FenetreNurikabe();
    FenetreNurikabe(int);
    FenetreNurikabe(int nbCaseParLigne,int nbCaseParColonne);
    FenetreNurikabe(int nbCaseParLigne,int nbCaseParColonne,int ** grille);
    //explicit FenetreNurikabe(QWidget *parent = nullptr);

signals:

public slots:
};

#endif // FENETRENURIKABE_H
