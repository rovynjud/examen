#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QFile>

#include "fenetrenurikabe.h"
#include <fstream>
#include <iostream>
#include <string>
int getNbrColonneGrilleV2(char *nomFichier){
    int nbrColonne=-1;
    std::ifstream fichier;
    fichier.open(nomFichier);
    char lecteurFichier;
    do{
        lecteurFichier=fichier.get();
        nbrColonne++;
    }while(lecteurFichier!='\n' && lecteurFichier!=':');
    fichier.close();
    return nbrColonne;
}
int getNbrColonneGrille(char * nomFichier){
    int nbrColonne=0;
    std::ifstream fichier;
    fichier.open(nomFichier);
    bool readingInteger=false;

    char lecteurFichier;
    do{
        lecteurFichier=fichier.get();
        if(lecteurFichier!='N' && lecteurFichier!='B' && lecteurFichier!=':' && lecteurFichier!='?'){
            if(!readingInteger){
                readingInteger=true;
            }
        }
        else{
            if(readingInteger){
                nbrColonne++;
            }
            readingInteger=false;
        }
        if(!readingInteger && lecteurFichier!=':')
            nbrColonne++;
    }while(lecteurFichier!='\n' && lecteurFichier!=':');
    fichier.close();
    return nbrColonne;
}
int getNbrLigneGrille(char * nomFichier){
    int nbrLigne=1;
    std::ifstream fichier;
    fichier.open(nomFichier);

    char lecteurFichier;
    do{
        lecteurFichier=fichier.get();
        if(lecteurFichier==':')
            nbrLigne++;

    }while(lecteurFichier!='\n');
    fichier.close();
    return nbrLigne;
}

int ** chargementGrille(char * nomFichier){
    int nbrLigne=getNbrLigneGrille(nomFichier);
    int nbrColonne=getNbrColonneGrille(nomFichier);
    std::ifstream fichier;
    fichier.open(nomFichier);
    int ** grille=new int*[nbrLigne];
    for(int i=0;i<nbrLigne;i++)
    {
        grille[i]=new int[nbrColonne];
    }


    /**
    * Fonctionnement : Si le caractère lu est un nombre, on ne le marque pas tout de suite dans la grille
    * car il pourrait y avoir le nombre 13, qui prend 2 caractères. Une fois qu'on tombe sur autre chose
    * qu'un nombre, on remet la chaine nombre = la chaîne vide.
    * Si on tombe sur N ou B, on inscrit ça dans la grille
    */
    int indiceLigne=0;
    int indiceColonne=0;
    std::string nombre="";
    char lecteurFichier;
    do{
        lecteurFichier=fichier.get();
        if(lecteurFichier!=EOF)
        {
            if(lecteurFichier!= ':')
            {
                if(lecteurFichier== 'N' || lecteurFichier=='B' || lecteurFichier=='?')
                {

                    if(nombre!="")
                    { // nombre != de la chaine vide <=> il y a un nombre en 'stand by' à marquer dans la grille
                        grille[indiceLigne][indiceColonne]=std::atoi(nombre.c_str());
                        nombre="";
                        indiceColonne++;
                    }
                    if(lecteurFichier== 'N')
                    {
                        grille[indiceLigne][indiceColonne]=-1;
                    }
                    else if(lecteurFichier== 'B')
                    {
                        grille[indiceLigne][indiceColonne]=0;
                    }
                    else{
                      grille[indiceLigne][indiceColonne]=-2;

                    }
                    indiceColonne++;
                }
                else{
                    nombre+=lecteurFichier;
                }
            }
            else{
                if(nombre!="")
                {
                    grille[indiceLigne][indiceColonne]=std::atoi(nombre.c_str());
                    nombre="";
                }
                indiceLigne++;
                indiceColonne=0;
            }
        }
    }while(lecteurFichier != '\n');
    fichier.close();
    return grille;
}
int ** chargementGrilleV2(char * nomFichier){
    int nbrLigne=getNbrLigneGrille(nomFichier);
    int nbrColonne=getNbrColonneGrille(nomFichier);
    std::ifstream fichier;
    fichier.open(nomFichier);
    int ** grille=new int*[nbrLigne];
    int indiceLigne=0;
    int indiceColonne=0;
    for(int i=0;i<nbrLigne;i++)
    {
        grille[i]=new int[nbrColonne];
    }
    char lecteurFichier;
    do{
        lecteurFichier=fichier.get();
        if(lecteurFichier!=':' && lecteurFichier!='\n'){
            if(lecteurFichier== 'N')
            {
                grille[indiceLigne][indiceColonne]=-1;
            }
            else if(lecteurFichier== 'B')
            {
                grille[indiceLigne][indiceColonne]=0;
            }
            else if(lecteurFichier== '?'){
              grille[indiceLigne][indiceColonne]=-2;
            }
            else{
                grille[indiceLigne][indiceColonne]=lecteurFichier;
            }
            indiceColonne++;
        }
        else{
            indiceLigne++;
            indiceColonne=0;
        }

    }while(lecteurFichier != '\n');
    fichier.close();
    return grille;
}


int main(int argc, char *argv[])
{
    std::string nomFichier="grille.txt";
    char *conversion=new char[nomFichier.length() + 1];
    strcpy(conversion,nomFichier.c_str());

    int nbrLigneFichier=getNbrLigneGrille(conversion);
    int nbrColonneFichier=getNbrColonneGrilleV2(conversion);
    std::cout<<nbrLigneFichier<<std::endl<<nbrColonneFichier<<std::endl;
    int **grille=chargementGrilleV2(conversion);
    /*
    grille[0]=new char[3];
    grille[1]=new char[3];
    grille[2]=new char[3];

    grille[0][0]=1;
    grille[0][1]='N';
    grille[0][2]='B';

    grille[1][0]='N';
    grille[1][1]='N';
    grille[1][2]=2;

    grille[2][0]=1;
    grille[2][1]='N';
    grille[2][2]='N';*/
    QApplication app(argc, argv);

   FenetreNurikabe fenetre(nbrColonneFichier,nbrLigneFichier,grille);
   fenetre.show();
    delete[] conversion;
    return app.exec();
}
